/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */




import 'dart:async';
import 'dart:convert';
import 'package:elrepo_lib/constants.dart';
import 'package:elrepo_lib/models.dart';
import 'package:retroshare_dart_wrapper/rs_models.dart';

import 'constants.dart' as cnst;
import 'package:retroshare_dart_wrapper/src/retroshare.dart' as rs;
import 'package:retroshare_dart_wrapper/rs_models.dart' as rs_models;
import 'package:elrepo_lib/models.dart' as models;
import 'package:mime/mime.dart' as mime;
import 'package:path/path.dart' as path;
import 'package:http/http.dart' as http;
import 'package:elrepo_lib/src/cache.dart';

part 'repo_lib/account_management.dart';
part 'repo_lib/circles.dart';
part 'repo_lib/download.dart';
part 'repo_lib/elrepo_gateway.dart';
part 'repo_lib/forums_bookmark.dart';
part 'repo_lib/forums_creation.dart';
part 'repo_lib/forums_get.dart';
part 'repo_lib/forums_subscription.dart';
part 'repo_lib/identities.dart';
part 'repo_lib/search.dart';
part 'repo_lib/forums_utils.dart';
part 'repo_lib/peer_management.dart';

// todo(kon) not used?
/// Return a list of real posts from a list of postHeaders that contain referred
/// content.
///
/// Example: you want to retrieve post headers from CONTENT forums using
/// BOOKMARKS post headers.
// Future<List> getContentPostsFromReferreds(List postHeaders) async {
//   print('Get post headers from referred contents list');
//   var forumsToPosts = getReferredsFromPostHeader(postHeaders);
//   var msgMeta = <dynamic>[];
//   for (var forumId in forumsToPosts.keys) {
//     var temp = await rs.RsGxsForum.getForumMsgMetaData(forumId);
//     temp.removeWhere((post) => !forumsToPosts[forumId].contains(post.mMsgId));
//     msgMeta.addAll(temp);
//   }
//   return msgMeta;
// }



/// From a post metadata object return referred content real post id
// dynamic getRealPostId(postMetadata) {
//   try {
//     final realPostId = postMetadata.referred.split(' ')[1];
//     return realPostId;
//   } catch (error) {
//     print('Could not retrieve original post: $error');
//     return null;
//   }
// }

// TODO(nicoechaniz): the cache implementation is very naive, it's there just as an experiment.
//  We need to implement this with sqlite storage or something similar
// List bookmarksCache;
// List selfPostsCache;
// Duration cacheDuration = Duration(seconds: 30);
// DateTime bookmarksLastRefresh;
// DateTime selfPostsLastRefresh;

// Future<List<dynamic>> getBookmarkedIds() async {
//   bookmarksLastRefresh ??= DateTime.now();
//   var timeDelta = DateTime.now().difference(bookmarksLastRefresh);
//   if (timeDelta < cacheDuration && bookmarksCache != null) {
//     print('------------------ return bookmarks from cache');
//     return bookmarksCache;
//   }
//   List bookmarkedIds;
//   final allForums = await rs.RsGxsForum.getForumsSummaries();
//   final bookmarkForumName = _getUserBookmarkName();
//   final bookmarksForum =
//       allForums.where((i) => i['mGroupName'] == bookmarkForumName).toList();
//   if (bookmarksForum.isNotEmpty) {
//     final bookmarkedMessages =
//         await rs.RsGxsForum.getForumMsgMetaData(bookmarksForum[0]['mGroupId']);
//     bookmarkedIds = bookmarkedMessages
//         .map((item) =>
//             getRealPostId(models.PostMetadata.fromJsonString(item.mMsgName)))
//         .toList();
//   }
//   bookmarksCache = bookmarkedIds;
//   bookmarksLastRefresh = DateTime.now();
//   return bookmarkedIds;
// }

// NOT USED
// Future<List<dynamic>> getSelfPostIds() async {
//   if (selfPostsLastRefresh == null) {
//     selfPostsLastRefresh = DateTime.now();
//   }
//   Duration timeDelta = DateTime.now().difference(selfPostsLastRefresh);
//   if (timeDelta < cacheDuration && selfPostsCache != null) {
//     return selfPostsCache;
//   }
//   List selfPostsIds;
//   final allForums = await rs.RsGxsForum.getForumsSummaries();
//   print("Fetching User posts");
//   final userForumName = _getUserForumName();
//   final userForum =
//     allForums.where((i) => i["mGroupName"] == userForumName).toList();
//   if (userForum.length > 0) {
//     final userMessages =
//       await rs.RsGxsForum.getForumMsgMetaData(userForum[0]["mGroupId"]);
//     selfPostsIds = userMessages
//         .map((item) =>
//         getRealPostId(models.PostMetadata.fromJsonString(item["mMsgName"])))
//         .toList();
//   }
//   selfPostsCache = selfPostsIds;
//   selfPostsLastRefresh = DateTime.now();
//   return selfPostsIds;
// }



