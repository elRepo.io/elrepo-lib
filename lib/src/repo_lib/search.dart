/*
 *  elRepo.io decentralized culture repository
 *
 *  Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 *  This program is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU Affero General Public License as published by the
 *  Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *  FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 *  SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 *  SPDX-License-Identifier: AGPL-3.0-only
 *
 */

part of '../repo.dart';

/// Use the perceptual search API to search by image
///
/// [localFilePath] is the file path and [distance] a number between 0 and 32
/// where 0 is all equal and 32 all different. A big difference, for example is
/// 10. So between 0 and 10 the images could be similar.
///
/// [callback] is the function to execute when a event of type
/// [RsEventType.FILE_PERCEPTUAL_SEARCH_RESULT_RECEIVED] i received on RsEvents
/// listener.
Future<StreamSubscription> perceptualSearch(String localFilePath,
    {int distance = 10, Function(dynamic) callback}) async {
  var searchId =
  await rs.RsFiles.perceptualSearchRequest(localFilePath, distance);
  return await rs.RsEvents.registerEventsHandler(
      RsEventType.FILE_PERCEPTUAL_SEARCH_RESULT_RECEIVED,
          (StreamSubscription stream, Map<String, dynamic> event) {
        print('Found perceptualSearchRequest!');
        print(event);
        if (event['mSearchId'] == searchId) {
          print('perceptualSearchRequest registered with searchId $searchId , '
              'calling callback if exist');
          if (callback != null) {
            callback(event['mResults']);
          }
        }
      });
}

/// Distant forum search. Ask to online nodes for a String.
///
/// Wrapper that return a Stream Subscription. Where [callback] argument is a
/// function that receive the events with the same search code. IMPORTANT: it
/// return only CONTENT forums
Future<StreamSubscription> distantSearch(
    String text, Function(List<RsMsgMetaData>) callback) async {
  var searchId = await rs.RsGxsForum.distantSearchRequest(text);
  return await rs.RsEvents.registerEventsHandler(RsEventType.GXS_FORUMS,
          (StreamSubscription stream, Map<String, dynamic> event) {
        if (event['mForumEventCode'] ==
            RsForumEventCode.DISTANT_SEARCH_RESULT.index &&
            event['mSearchId'] == searchId) {
          print('Found search event!');
          print(event);
          if (callback != null) {
            callback([
              // Delete non CONTENT forums
              for (var post in event['mSearchResults'])
                if (post['mGroupName'] == getForumName('CONTENT')) RsMsgMetaData.fromJson(post)
            ]);
          }
        }
      });
}

/// Local search wrapper that return sorted list of local searched posts
///
/// Order the post by following and return only content posts
Future<List<RsMsgMetaData>> sortedLocalSearch(String text) async {
  return await sortPostList([
    for (var post in await rs.RsGxsForum.localSearch(text))
      if (post['mGroupName'] == getForumName('CONTENT'))
        RsMsgMetaData.fromJson(post)
  ]);
}