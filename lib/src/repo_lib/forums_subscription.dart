/*
 *  elRepo.io decentralized culture repository
 *
 *  Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 *  This program is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU Affero General Public License as published by the
 *  Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *  FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 *  SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 *  SPDX-License-Identifier: AGPL-3.0-only
 *
 */

part of '../repo.dart';

/// Subscribe to specific `forum['mGroupId']`
///
/// It also `setStoragePeriod` and `setSyncPeriod` to [cnst.MAX_INT]. Finally it
/// do [rs.RsGxsForum.requestSynchronization]
void subscribeToForum(String mGroupId, [bool subscribe = true]) async {
  await rs.RsGxsForum.subscribeToForum(mGroupId, subscribe);
  if (subscribe) {
    rs.RsGxsForum.setStoragePeriod(mGroupId, cnst.MAX_INT);
    rs.RsGxsForum.setSyncPeriod(mGroupId, cnst.MAX_INT);
    rs.RsGxsForum.requestSynchronization();
  }
}

/// Subscribe to a list of forums
Future<List<dynamic>> subscribeToForums(List<dynamic> forums, [bool subscribe = true]) async {
  for (var forum in forums) {
    subscribeToForum(forum['mGroupId'], subscribe);
    print('Subscribed to $forum');
  }
  return forums;
}

/// Subscribe to all repo forums
///
/// Repo forums are thoose that accomplish: `elRepo_${cnst.API_VERSION}_`. It
/// loops [rs.RsGxsForum.getForumsSummaries] subscribing all.
Future<dynamic> subscribeToRepoForums() async {
  final allForums = await rs.RsGxsForum.getForumsSummaries();
  final elRepoForums = allForums
      .where((i) => i['mGroupName'] == 'elRepo_${cnst.API_VERSION}_')
      .toList();
  print('Found elRepo.io forums $elRepoForums');
  Map forum;
  for (forum in elRepoForums) {
// TODO(nicoechaniz): correctly filter depending on forum["mSubscribeFlags"] values
    subscribeToForum(forum['mGroupId'], true);
  }
}