/*
 *  elRepo.io decentralized culture repository
 *
 *  Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 *  This program is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU Affero General Public License as published by the
 *  Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *  FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 *  SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 *  SPDX-License-Identifier: AGPL-3.0-only
 *
 */

part of '../repo.dart';

/// Get a list of downloading files details
Future<List<dynamic>> getDownloadingListDetails() async {
  var hashes = await rs.RsFiles.fileDownloads();
  return [for (String hash in hashes) await rs.RsFiles.fileDetails(hash)];
}

/// Return the filename with the full absolute path if it exists on the file
/// system and the retroshare download status code is 4 (FT_STATE_COMPLETE)
///
/// It could take a while to get the path because RS do something before change
/// the download status, even if on [rs.RsFiles.fileDetails] the available size
/// and file size are the same.
///
/// Return `fileInfo["path"]` if response is a map else return null (if retval
/// is not `true`)
Future<String> getPathIfExists(hash) async {
  final fileInfo = await rs.RsFiles.alreadyHaveFile(hash);
  return fileInfo is Map ? fileInfo['path'] : null;
}

/// Set download and upload max rates on Retroshare
Future setMaxDataRates(int downKb , int upKb) async {
  if (await rs.isRetroshareRunning() && await rs.RsLoginHelper.isLoggedIn()) {
    await rs.RsConfig.setMaxDataRates(10000, 10000);
  }
}

/// Set online mode presuming default value of 10000 [downKb] and [upKb]
///
/// It just call [setMaxDataRates], you could call directly there with desired
/// values.
Future onlineMode({int downKb = 10000, int upKb = 10000}) async =>
  setMaxDataRates(downKb, upKb);

/// Set offline mode presuming default value of 500 [downKb] and [upKb]
///
/// It just call [setMaxDataRates], you could call directly there with desired
/// values.
Future offlineMode({int downKb = 500, int upKb = 500}) async =>
    setMaxDataRates(downKb, upKb);