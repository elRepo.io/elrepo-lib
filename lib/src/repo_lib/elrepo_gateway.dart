/*
 *  elRepo.io decentralized culture repository
 *
 *  Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 *  This program is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU Affero General Public License as published by the
 *  Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *  FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 *  SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 *  SPDX-License-Identifier: AGPL-3.0-only
 *
 */

part of '../repo.dart';


// ********** elrepo.io gateway related functions ********** //

models.GatewayAuthObject originalAuth;

/// Function that enables comunication to a remote RS
Future<bool> changeRsPrefix(String newRsPrefix) async {
  rs.setRetroshareServicePrefix(newRsPrefix);
  if (!await rs.isRetroshareRunning()) {
    rs.setRetroshareServicePrefix();
    print("Can't connect to remote RS");
    return false;
  }
  print('The new RS prefix is: ' + rs.getRetroshareServicePrefix());
  return true;
}

/// Call to an aqueduct elrepo.io instance to get an authorization token.
///
/// This is used on the remote control workflow for elrepo.io. Where
/// [newRsPrefix] should be something like: 'http://aqueductIp:8888/rsremote/'
/// The received user/pass for the specified [apiUser] will be used as new auth token
Future<bool> requestGatewayAuthorization(
    String newRsPrefix, String apiUser) async {
  try {
    // Here the POST api call to aqueductIp:8888/rsremote/:newUser
    final res = await http.post(Uri.parse(newRsPrefix + '/' + apiUser));
    // The response is a username and a new random password associated
    if (res != null && res.statusCode == 200) {
      var decoded = models.GatewayAuthObject.fromJson(jsonDecode(res.body));

      // If original auth == null store it. This should prevent to don't lose the
      // original auth if requestGatewayAuthorization() executed twice or more
      originalAuth ??= models.GatewayAuthObject(
          rs.authApiUser, rs.authPassphrase, rs.authIdentityId);

      rs.initRetroshare(
          apiUser: decoded.user,
          passphrase: decoded.password,
          identityId: decoded.identity);
      return true;
    }
  } catch (err) {
    print('Cant get new auth token from elrepo.io gateway');
    return false;
  }
}

/// This funcion change on meory RsPrefix by [newRsPrefix] and request a new [apiUser]
/// token auth to talk, tipically, with an aqueduct elrepo.io gateway
Future<bool> enableRemoteControl(String newRsPrefix, String apiUser) async {
  var enableSuccess = await changeRsPrefix(newRsPrefix) &&
      await requestGatewayAuthorization(newRsPrefix, apiUser);
  if (!enableSuccess) disableRemoteControl();
  return enableSuccess;
}

/// Set the default values after enableRemoteControl() is executed
Future<void> disableRemoteControl() async {
  rs.setRetroshareServicePrefix();
  if (originalAuth != null) {
    rs.initRetroshare(
        apiUser: originalAuth.user,
        passphrase: originalAuth.password,
        identityId: originalAuth.identity);
    originalAuth = null;
  }
}

/// Check if an [ip] has the "rsremote" endpoint.
///
/// This should mean that is an aqueduct gateway node that accepts remote control.
///
// Todo(kon): an improvement could be to find the way to check if the IP is
// outside the local network in order to don't do the check.
Future<bool> supportRemoteControl(String ip) async {
  var remote = 'http://$ip:8888/rsremote';
  print('Check remote control for $remote');
  try {
    final response = await http.get(Uri.parse(remote));
    return (response == null || response?.statusCode != 200)
        ? false
        : jsonDecode(utf8.decode(response.bodyBytes))['retval'];
  } catch (e) {
    print(e);
    return false;
  }
}

/// Get user followed content based on BOOKMARKS and USER forum
Future<List> getSyncedContentMetadata() async {
  final allForums = await rs.RsGxsForum.getForumsSummaries();
  final userForum = allForums
      .where((i) =>
  i['mGroupName'] == _getUserForumName() ||
      i['mGroupName'] == _getUserBookmarkName())
      .toList();
  var userForumMetadatas = await _getForumListMetadata(userForum);
// Get refered content
  Map<String, List<String>> referredContent = {};
  userForumMetadatas.forEach((element) {
    var referred =
    element.postMetadata.referred.split(' ');
    referredContent[referred[0]] = referredContent[referred[0]]?.isEmpty ?? true
        ? [
      ...[referred[1]]
    ]
        : [
      ...referredContent[referred[0]],
      ...[referred[1]]
    ];
  });
  var resultingList = await Future.wait(
// This return a list of lists, so we have to expand it to get a single list of posts objects
      referredContent.entries
          .map((e) async => rs.RsGxsForum.getForumContent(e.key, e.value)));
  return resultingList.expand((i) => i).toList();
}

/// From a [forumList], return the list with all the posts metadata associated
Future<List<RsMsgMetaData>> _getForumListMetadata(List<dynamic> forumList) async {
  var allPosts = <dynamic>[];
  for (Map forum in forumList) {
    allPosts.addAll(await _getPostsMetadata(forum));
  }
  return allPosts;
}

/// From a [forum] object, return the metadata associated in a Post iterable
Future<Iterable<RsMsgMetaData>> _getPostsMetadata(Map forum) async {
  var posts = await rs.RsGxsForum.getForumMsgMetaData(forum['mGroupId']);
  return posts.where((post) {
// Try to parse PostMetadata.fromJsonString to prevent malformed json
    try {
      var postMetadata = models.PostMetadata.fromJsonString(post.mMsgName);
      return postMetadata.role == PostRoles.post;
    } on Exception {
      return false;
    }
  });
}

Future<String> getLocationPath() async {
  var locationPath = '';
  final sharedDirs = await rs.RsFiles.getSharedDirectories();
  if (sharedDirs.isNotEmpty) {
    final downloadPath = sharedDirs[0]['filename'];
    final pathComponents = path.split(downloadPath);
    pathComponents.removeLast();
    locationPath = path.joinAll(pathComponents);
  }
  return locationPath;
}

Future<Map> getLocation() async {
  final location = await rs.RsLoginHelper.getDefaultLocation();
  return location;
}
